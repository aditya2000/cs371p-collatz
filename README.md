# CS371p: Object-Oriented Programming Collatz Repo

* Name: Aditya Gupta

* EID: ag68834

* GitLab ID: aditya2000

* HackerRank ID: aditya20001

* Git SHA: 373291d5f1fd1b47327123812cdd55595a78dbfe

* GitLab Pipelines: https://gitlab.com/aditya2000/cs371p-collatz/-/pipelines

* Estimated completion time: 9 hours

* Actual completion time: 15 hours

* Comments: This was really interesting and a great learning opportunity.
