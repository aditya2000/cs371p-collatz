// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

TEST(CollatzFixture, read2) {
    ASSERT_EQ(collatz_read("999999 1\n"), make_pair(999999, 1));
}


// -----------
// get_cyc_len
// -----------

TEST(CollatzFixture, get_cyc_len0) {
    ASSERT_EQ(get_cyc_len(5), 6);
}

TEST(CollatzFixture, get_cyc_len1) {
    ASSERT_EQ(get_cyc_len(1), 1);
}

TEST(CollatzFixture, get_cyc_len2) {
    ASSERT_EQ(get_cyc_len(128), 8);
}

TEST(CollatzFixture, get_cyc_len3) {
    ASSERT_EQ(get_cyc_len(999999), 259);
}

TEST(CollatzFixture, get_cyc_len3caching) {
    ASSERT_EQ(get_cyc_len(999999), 259);
}

TEST(CollatzFixture, get_cyc_len4) {
    ASSERT_EQ(get_cyc_len(29328), 47);
}

TEST(CollatzFixture, get_cyc_len5) {
    ASSERT_EQ(get_cyc_len(1000), 112);
}

TEST(CollatzFixture, get_cyc_len6) {
    ASSERT_EQ(get_cyc_len(3), 8);
}

TEST(CollatzFixture, get_cyc_len7) {
    ASSERT_EQ(get_cyc_len(999993), 166);
} // can cause an overflow


// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 900)), make_tuple(1000, 900, 174));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 525));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(999, 1001)), make_tuple(999, 1001, 143)); 
}



// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

TEST(CollatzFixture, print2) {
    ostringstream sout;
    collatz_print(sout, make_tuple(999999, 1, 525));
    ASSERT_EQ(sout.str(), "999999 1 525\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}


